package SaveAsNew;

import genericPack.LibGenericClass;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;


public class SaveAsNewGenericClass {
	
	WebDriver driver;
	SaveAsNewLibGenericClass lib;
	
	//This function responsible for login to Jupiter
	@Test(priority = 1)
	public void login() throws IOException{
		lib = new SaveAsNewLibGenericClass(driver);
		lib.loginToJupiter();
	}
	
	@Test(priority =2)
	public void createAccommodationTypes() throws InterruptedException{
		lib.createAccomodationType();
	}
}

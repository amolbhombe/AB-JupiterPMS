package genericPack;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;



public class GenericCLass {
			WebDriver driver;
			LibGenericClass lib;
			
			//This function responsible for login to Jupiter
			@Test(priority = 1)
			public void login() throws IOException{
				lib = new LibGenericClass(driver);
				lib.loginToJupiter();
			}
			
			@Test(priority =2)
			public void enterData() throws InterruptedException{
				lib.enterData();
			}
			
}

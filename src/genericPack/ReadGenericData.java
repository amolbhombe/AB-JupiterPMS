package genericPack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadGenericData {
		HashMap<String, String> data;
		HashMap<String, String> menuFromExcel;
		File file;
		FileInputStream fis;
		XSSFWorkbook workbook;
		XSSFSheet sheet;
		XSSFSheet generalInformation;
		XSSFSheet menu;
		
		
		public  String chromeLocation;
		public	String URL;
		public	String username;
		public	String password;
		
		public int column;
		public String columnString;
		public String menuname;
		public ArrayList<String> allTestingmenu;
		
		//This the constructor of ReadGenericData class
		public ReadGenericData() throws IOException{
				data = new HashMap<String, String>();
				menuFromExcel = new HashMap<String, String>();
				file = new File("/home/amol/Music/AmolRRRR/JavaProjects/AB-JupiterPMS/JupiterCredentials&Data.xlsx");
				fis = new FileInputStream(file);
				workbook = new XSSFWorkbook(fis);
				sheet = workbook.getSheet("Generic");
				generalInformation = workbook.getSheet("generalInformation");
				menu = workbook.getSheet("MenuName");
				
				chromeLocation = generalInformation.getRow(0).getCell(1).getStringCellValue();
				URL = generalInformation.getRow(1).getCell(1).getStringCellValue();
				username = generalInformation.getRow(2).getCell(1).getStringCellValue();
				password = generalInformation.getRow(3).getCell(1).getStringCellValue();
		}
		
		//This function returns all the name and its value
		public HashMap<String, String> read(){
				for(int i=0; i<=sheet.getLastRowNum(); i++){
					try{
						data.put(sheet.getRow(i).getCell(1).getStringCellValue(), sheet.getRow(i).getCell(2).getStringCellValue());
					}
					catch(Exception e){//if the excel filed contains the number then catch executes
						column = (int)sheet.getRow(i).getCell(2).getNumericCellValue();
						columnString = Integer.toString(column);
						data.put(sheet.getRow(i).getCell(1).getStringCellValue(), columnString);
					}	
				}
			return data;
		}
		
		//This function returns the menu name which we want to click
		public HashMap<String, String> menuRead(){
			for(int j=0; j<=menu.getLastRowNum();j++){
				menuFromExcel.put(menu.getRow(j).getCell(0).getStringCellValue(),menu.getRow(j).getCell(1).getStringCellValue());
			}
			return menuFromExcel;
		}
}

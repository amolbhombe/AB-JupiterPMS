package update;


import genericPack.ReadGenericData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import jupiter_Pack1.ReadData;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class UpdateLibGenericClass {
	WebDriver driver;
	ReadGenericData getdata;
	WebDriverWait wait;
	String str;
	int sizeOfWebelement;
	String menuWhile;
	String menuWeWant;
	List<WebElement> allSettingMenu;
	String pageTitleBefore;
	
	
	By usename = By.id("loginform-username");
	By password = By.id("loginform-password");
	By loginButton = By.name("login-button");
	By createButton = By.xpath("//button[@class='btn btn-success'][contains(text(),'Create')]");
	By updateButton = By.xpath("//button[contains(text(),'Update')]");
	
	
	By SettingsMenu = By.xpath("//aside[@class='main-sidebar']//ul//li//a//span");
	By accommodationTypesMenu = By.xpath("//span[contains(text(),'Accommodation Types')]"); 
	By accommodationTypesCreateButton = By.xpath("//a[contains(text(),'Create Accommodation Type')]");
	
	
	
	
	
	//This is Constructor of LibGenericClass
		public UpdateLibGenericClass(WebDriver driver) throws IOException{
			this.driver = driver;
			getdata = new ReadGenericData();
		}
		
		//This function login to Jupiter Website
		public void loginToJupiter(){
			System.setProperty("webdriver.chrome.driver", getdata.chromeLocation );
			driver = new ChromeDriver();
			wait = new WebDriverWait(driver, 2);
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(getdata.URL);
			driver.findElement(usename).sendKeys(getdata.username);
			driver.findElement(password).sendKeys(getdata.password);
			driver.findElement(loginButton).click();
			getdata.menuRead();
		}
		
		public void createAccomodationType() throws InterruptedException {
			Logger logger = Logger.getLogger("LibGenericClass");
			PropertyConfigurator.configure("log4j.properties");
//			logger.info("Coming from logger");
			
			
			Set<String> keysetOfMenu = getdata.menuRead().keySet();//get all the keys from Menu name excel
			Iterator<String> itrMenu = keysetOfMenu.iterator();
			while(itrMenu.hasNext()){
				menuWhile =  itrMenu.next();
				logger.info("We are woking on "+menuWhile +" module");
				menuWeWant = getMenuFromExcel(menuWhile);
				if(menuWeWant.equalsIgnoreCase("underSettings")){
					List<WebElement> allSettingMenu = driver.findElements(SettingsMenu);
					Iterator<WebElement> web = allSettingMenu.iterator();
					while(web.hasNext()){
						WebElement ele_Settings = web.next();
							String str_settings = ele_Settings.getText();
							if(str_settings.equalsIgnoreCase("Settings")){
								ele_Settings.click();
								break;
							}
					}
					
				}
						String clickOnMenu = createXpathForMenu(menuWhile);
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(clickOnMenu))).click();;//driver.findElement(By.xpath(clickOnMenu));//
																			
						
						/*String clickOnCreateButton = createXpathForCreateButton(getdata.menuRead().get(menuWhile));
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(clickOnCreateButton))).click();*/
						
						WebElement ele_345 = driver.findElement(By.xpath("//table/tbody/tr[1]//td/a[3]/span"));
						wait.until(ExpectedConditions.elementToBeClickable(ele_345)).click();
						
						Set<String> keyset = getdata.read().keySet();//This code get all the Keys (i.e. all the name)
						Iterator<String> itrKeySet= keyset.iterator();
							while(itrKeySet.hasNext()){
								String gettingKeys = itrKeySet.next();// Here we get the keys
								sizeOfWebelement = driver.findElements(By.name(gettingKeys)).size();
								
								//we check the tagname if it is input and the type is text (i.e. Text field)then this code execute
								
								if(sizeOfWebelement !=0){
									WebElement keyWeBelement =	driver.findElement(By.name(gettingKeys));				
									String tagName = keyWeBelement.getTagName(); //from the keys we found the tag name 
									String attribute = keyWeBelement.getAttribute("type");		//select2-taxesfees-type-container //actual
																								//select2-taxesfees-type-container
									String classname = keyWeBelement.getAttribute("class");
																								//select2-taxesfees-applicable_on-container //actual
									switch(tagName){											//select2-applicable_on-container  //expected
									case "select": 
									//	System.out.println("Tagname is "+ tagName);
										String clickElements = clickConverter(gettingKeys,"click");//here we get the id of the select 2 dropdown from the select drop down
										driver.findElement(By.id(clickElements)).click(); //we click on drop down
										String gettingElements = clickConverter(gettingKeys,"getting");//here we get id from which we cab read all the elements
										String xpathConvetredString = xpathforDropDown(gettingElements); //we create the xpath to read all the elements
										List<WebElement> xpathConvetredStringWeb = driver.findElements(By.xpath(xpathConvetredString));
										Iterator<WebElement> itr = xpathConvetredStringWeb.iterator();
										while(itr.hasNext()){
											WebElement ele = itr.next();
												String code1 = 	ele.getText();
												if(code1.equalsIgnoreCase(getdata.read().get(gettingKeys))){//here we search a specific text from which we can click on specific menu
												ele.click();
													break;
												}
											}
													break;
									case "input": 
													switch(attribute){
													case "text":
																switch(classname){
																case "form-control": keyWeBelement.clear();
																					 wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
																					 break;
																case "form-control krajee-datepicker": //System.out.println("We are in date field");
																										driver.findElement(By.name(gettingKeys)).clear();
																										Thread.sleep(1000);
																										driver.findElement(By.name(gettingKeys)).click();
																										List<WebElement> calender = driver.findElements(By.xpath("//table/tbody//tr//td"));
																										Iterator<WebElement> itrCalender = calender.iterator();
																										while(itrCalender.hasNext()){
																											WebElement WebelementCalnder = itrCalender.next();
																											String calenderStringelement = WebelementCalnder.getText();
																										//	System.out.println("calender element is "+calenderStringelement);
																												if(calenderStringelement.equalsIgnoreCase(getdata.read().get(gettingKeys))){
																										//		System.out.println("I am in if");
																												String date = WebelementCalnder.getText();
																												wait.until(ExpectedConditions.elementToBeClickable(WebelementCalnder)).click();
																												Thread.sleep(2000);
																												break;
																											}
																										}
																												break;
																}
																break;
													case "hidden":
																	switch(getdata.read().get(gettingKeys)){
																	case "No":  String xpathActive = xpathforActiveToggle(gettingKeys);
																				WebElement eleforDropDown = driver.findElement(By.xpath(xpathActive));
																				JavascriptExecutor executor = (JavascriptExecutor)driver;
																				executor.executeScript("arguments[0].click();", eleforDropDown);
																				break;
																}
																break;
													case "number":  keyWeBelement.clear();
																	wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
																break;
																	
													case "password": keyWeBelement.clear();
																	 wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field							
																break;
													}
																break;
									default : System.out.println("I am in default");
									}
											}else{
												continue;
											}
								}						
							
								pageTitleBefore = driver.getTitle();
								JavascriptExecutor demo = (JavascriptExecutor)driver;
								demo.executeScript("scroll(0, 800)");
								wait.until(ExpectedConditions.elementToBeClickable(updateButton)).click();//Here we click on create button
								
								
								
								
								Thread.sleep(2000);
								//System.out.println("Page title is "+ driver.getTitle());
							//	logger.info("Page title is "+driver.getTitle());
								if(driver.getTitle().equalsIgnoreCase(pageTitleBefore)){
									logger.info("Update functionality of " +menuWhile +" is not working");
								}
								else{
									logger.info("Update functionality of " +menuWhile +" is working fine");
								}
								demo.executeScript("scroll(800,0)");
								if(menuWeWant.equalsIgnoreCase("underSettings")){
									allSettingMenu = driver.findElements(SettingsMenu);
									Iterator<WebElement> web = allSettingMenu.iterator();
									while(web.hasNext()){
										WebElement ele_Settings = web.next();
											String str_settings = ele_Settings.getText();
											if(str_settings.equalsIgnoreCase("Settings")){
												ele_Settings.click();
												break;
											}
									}
									
								}
										Thread.sleep(2000);
			}
		}
							//This function found the drop down id, on which we want to click also and also found  all the elements id from the drop down 
							public String clickConverter(String replaceString,String whichString){
								String str = replaceString;
						        str = str.replaceAll("\\[", "-");
						        str = str.replaceAll("\\]", "-");
						        str = str.toLowerCase();
								        if(whichString.equalsIgnoreCase("click")){
								        str = "select2-"+str+"container";
								        }
								        else{
								        	 str = "select2-"+str+"results";
								        }
						        return str;
							}
			
							//This function create the xpath to select all the elements from the drop down
							public String xpathforDropDown(String xpathstr){
								xpathstr = "//span//ul[@id='" + xpathstr + "']/li";
								return xpathstr;
							}
			
							//This function create the xpath for Active toggle 
							public String xpathforActiveToggle(String xpathtoggle){
						        xpathtoggle = xpathtoggle.replaceAll("\\[", "-");
						        xpathtoggle = xpathtoggle.replaceAll("\\]", "");
						        xpathtoggle = xpathtoggle.toLowerCase();
						        xpathtoggle = "//input[@id='"+xpathtoggle+"']/preceding::span[3]";
						        return xpathtoggle;
							}
			
			
							public String createXpathForMenu(String clickOnMenu){
								clickOnMenu = "//span[contains(text(),'"+clickOnMenu+"')]";
								return clickOnMenu;
							}


							public String createXpathForCreateButton(String clickOnCreateButton){
								clickOnCreateButton = "//a[contains(text(),'"+clickOnCreateButton+"')]";
								return clickOnCreateButton;
							}
			
							public String getMenuFromExcel(String accessMenu){
								str = "outSideofSettings";
								ArrayList<String> sortMenu = new ArrayList<String>();
								sortMenu.add("Menus");
								sortMenu.add("Roles");
								sortMenu.add("Menus Roles Permissions");
								sortMenu.add("Accommodation Types");
								sortMenu.add("Cancellation Policies");
								sortMenu.add("Card Types");
								sortMenu.add("Charge Types");
								sortMenu.add("Item Categories");
								sortMenu.add("Payment Options");
								sortMenu.add("Permissions");
								sortMenu.add("Statuses");
								sortMenu.add("Parameters");
								sortMenu.add("General Settings");
								
								Iterator<String> itrgetMenuFromExcel = sortMenu.iterator();
								while(itrgetMenuFromExcel.hasNext()){
									String getStr = itrgetMenuFromExcel.next();
									if(accessMenu.equalsIgnoreCase(getStr)){
										str = "underSettings";
										break;
									}
								}
									return str;
							}
							
}

package update;

import genericPack.LibGenericClass;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class UpdateGenericClass {
	WebDriver driver;
	UpdateLibGenericClass lib;
	
	//This function responsible for login to Jupiter
	@Test(priority = 1)
	public void login() throws IOException{
		lib = new UpdateLibGenericClass(driver);
		lib.loginToJupiter();
	}
	
	@Test(priority =2)
	public void createAccommodationTypes() throws InterruptedException{
		lib.createAccomodationType();
	}
}

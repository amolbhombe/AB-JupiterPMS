package jupiter_Pack1;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LibClassJupiter {
	WebDriver driver;
	ReadData getdata;
	WebDriverWait wait;
	
	
	//Login page element
	By usename = By.id("loginform-username");
	By password = By.id("loginform-password");
	By loginButton = By.name("login-button");
	
	//Global element
	By SettingsMenu = By.xpath("//aside[@class='main-sidebar']//ul//li//a//span");
	By createButton = By.xpath("//button[@class='btn btn-success'][contains(text(),'Create')]");
	
	//Accommodation Types
	By accommodationTypesMenu = By.xpath("//span[contains(text(),'Accommodation Types')]");
	By accommodationTypesCreateButton = By.xpath("//a[contains(text(),'Create Accommodation Type')]");
	By nameAccommodationTypes = By.id("nameACCOMODATIONTYPESsource");
	By codeAccommodationTypes = By.id("accomodationtypes-code");
	By typeAccommodationTypesClickOnButton = By.id("select2-accomodationtypes-type-container");
	By typeAccommodationTypes = By.xpath("//span[@class='select2-results']/ul/li");
	By numberofUnitsAccommodationTypes = By.id("accomodationtypes-number_of_units");
	By standerdAdultsAccommodationTypes = By.id("accomodationtypes-standard_adults");
	By standerdChildsAccommodationTypes = By.id("accomodationtypes-standard_child");
	By maximumAdultsAccommodationTypes = By.id("accomodationtypes-max_adults");
	By allowedGenderAccommodationTypesClickOnButton = By.id("select2-accomodationtypes-allowed_gender-container");
	By allowedGenderAccommodationTypes = By.xpath("//div[@class='row customCheckbox']/div[@class='col-md-3']/label");
	By descriptionAccommodationTypes = By.id("descriptionACCOMODATIONTYPESsource");
	By amenityAccommodationTypes = By.xpath("//div[@class='row customCheckbox']/div[@class='col-md-3']/label");
	
	//Constructor of LibClassJupiter class
	public LibClassJupiter(WebDriver driver) {
		this.driver = driver;
		
	}
	
	//Login to Jupiter
	public void loginToJupiter() {
			try {
			getdata = new ReadData();
			} catch (Exception  e) {
			System.out.println("This is getdata catch"+e.getMessage());
			}
			
			Logger logger = Logger.getLogger("Jupiter.java");
			PropertyConfigurator.configure("log4j.properties");
			
			System.setProperty("webdriver.chrome.driver", getdata.chromeLocation);
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(getdata.URL);
			logger.info("URL open and running");
			driver.findElement(usename).sendKeys(getdata.username);
			driver.findElement(password).sendKeys(getdata.password);
			driver.findElement(loginButton).click();
			logger.info("This is coming from cunstructor throuth log4j");
			
			}
	
			//Create Accommodation Types
			public void libCreateAccoType() throws InterruptedException{
				List<WebElement> allSettingMenu = driver.findElements(SettingsMenu);
				Iterator<WebElement> web = allSettingMenu.iterator();
				while(web.hasNext()){
					WebElement ele_Settings = web.next();
						String str_settings = ele_Settings.getText();
						if(str_settings.equalsIgnoreCase("Settings")){
							ele_Settings.click();
						}
				}
				
				driver.findElement(accommodationTypesMenu).click();
				wait = new WebDriverWait(driver, 3);
				WebElement create1 = wait.until(ExpectedConditions.elementToBeClickable(accommodationTypesCreateButton));
				create1.click();
				
				driver.findElement(nameAccommodationTypes).sendKeys(getdata.nameAccommodationTypes);
				driver.findElement(codeAccommodationTypes).sendKeys(getdata.codeAccommodationTypes);
				
				driver.findElement(typeAccommodationTypesClickOnButton).click();
				List<WebElement> webelementTypeAccommodationTypes = driver.findElements(typeAccommodationTypes);
				Iterator<WebElement> iteratorTypeAccommodationTypes = webelementTypeAccommodationTypes.iterator();
				while(iteratorTypeAccommodationTypes.hasNext()){
					WebElement ele = iteratorTypeAccommodationTypes.next();
					String code1 = 	ele.getText();
					if(code1.equalsIgnoreCase(getdata.typeAccommodationTypes)){
					ele.click();
					break;
					}
					
				}
				
				driver.findElement(numberofUnitsAccommodationTypes).sendKeys(getdata.numberofUnitsAccommodationTypesAsString);
				driver.findElement(standerdAdultsAccommodationTypes).sendKeys(getdata.standerdAdultsAccommodationTypesAsString);
				driver.findElement(standerdChildsAccommodationTypes).sendKeys(getdata.standerdChildsAccommodationTypesAsString);
				driver.findElement(maximumAdultsAccommodationTypes).sendKeys(getdata.maximumAdultsAccommodationTypesAsString);
				
				
				driver.findElement(allowedGenderAccommodationTypesClickOnButton).click();
				List<WebElement> webElementGenderAccommodationTypes = driver.findElements(allowedGenderAccommodationTypes);
				Iterator<WebElement> iteratorGenderAccommodationTypes = webElementGenderAccommodationTypes.iterator();
				while(iteratorGenderAccommodationTypes.hasNext()){
					WebElement ele = iteratorGenderAccommodationTypes.next();
					String gender1 = ele.getText();
					if(getdata.allowedGenderAccommodationTypes.equalsIgnoreCase(gender1)){
						ele.click();
						break;
					}
				}
				
				driver.findElement(descriptionAccommodationTypes).sendKeys(getdata.descriptionAccommodationTypes);
				
				
				List<WebElement> elementsAmenityAccommodationTypes_1 = driver.findElements(allowedGenderAccommodationTypes);
				Iterator<WebElement> iteratorAmenityAccommodationTypes = elementsAmenityAccommodationTypes_1.iterator();
				while(iteratorAmenityAccommodationTypes.hasNext()){
					WebElement elementsAmenityAccommodationTypes_2 =iteratorAmenityAccommodationTypes.next();
					String amenityString = elementsAmenityAccommodationTypes_2.getText();
					if(getdata.amenityAccommodationTypes.equalsIgnoreCase(amenityString)){
						elementsAmenityAccommodationTypes_2.click();
						break;
					}
				}
				
				driver.findElement(createButton).click();
				Thread.sleep(2000);
				String pageTitle = driver.getTitle();
				System.out.println("Page title is "+pageTitle);
				Assert.assertEquals(pageTitle,getdata.nameAccommodationTypes);
	}

}

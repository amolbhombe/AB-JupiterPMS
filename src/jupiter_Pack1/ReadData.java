package jupiter_Pack1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadData {
		//Global setting element
		String chromeLocation;
		String temp_AddedFromLocal;
		String temp_AddedFromServer;
		String URL;
		File file;
		FileInputStream fis;
		XSSFWorkbook workbook;
		XSSFSheet generalInformation;
		XSSFSheet AccommodationTypes;
		XSSFSheet generic;
		//Login element
		String username;
		String password;
		
		//Accommodation Types Element
		String nameAccommodationTypes;
		String codeAccommodationTypes;
		String typeAccommodationTypes;
		
		int numberofUnitsAccommodationTypes;
		String numberofUnitsAccommodationTypesAsString;
		int standerdAdultsAccommodationTypes;
		String standerdAdultsAccommodationTypesAsString;
		int standerdChildsAccommodationTypes;
		String standerdChildsAccommodationTypesAsString;
		int maximumAdultsAccommodationTypes;
		String maximumAdultsAccommodationTypesAsString;
		String allowedGenderAccommodationTypes;
		
		String descriptionAccommodationTypes;
		String amenityAccommodationTypes;
		

			
		//Constructor of ReadData
		public ReadData() {
		try {
			file = new File("/home/amol/Music/AmolRRRR/JavaProjects/AB-JupiterPMS/JupiterCredentials&Data.xlsx");
			fis = new FileInputStream(file);
			workbook = new XSSFWorkbook(fis);
			generalInformation = workbook.getSheetAt(0);
			AccommodationTypes = workbook.getSheetAt(1);
			generic = workbook.getSheet("Generic");
			
			int lastRow = generic.getLastRowNum();
			System.out.println("Last row is "+lastRow);
				
			
			//login page element
			chromeLocation = generalInformation.getRow(0).getCell(1).getStringCellValue();
			URL = generalInformation.getRow(1).getCell(1).getStringCellValue();
			username = generalInformation.getRow(2).getCell(1).getStringCellValue();
			password = generalInformation.getRow(3).getCell(1).getStringCellValue();
			
			
			//Accommodation Types Element
			nameAccommodationTypes = AccommodationTypes.getRow(0).getCell(1).getStringCellValue();
			codeAccommodationTypes = AccommodationTypes.getRow(1).getCell(1).getStringCellValue();
			typeAccommodationTypes = AccommodationTypes.getRow(2).getCell(1).getStringCellValue();
			numberofUnitsAccommodationTypes = (int) AccommodationTypes.getRow(3).getCell(1).getNumericCellValue();
			numberofUnitsAccommodationTypesAsString = Integer.toString(numberofUnitsAccommodationTypes);
			standerdAdultsAccommodationTypes = (int) AccommodationTypes.getRow(4).getCell(1).getNumericCellValue();
			standerdAdultsAccommodationTypesAsString = Integer.toString(standerdAdultsAccommodationTypes);
			standerdChildsAccommodationTypes = (int) AccommodationTypes.getRow(5).getCell(1).getNumericCellValue();
			standerdChildsAccommodationTypesAsString = Integer.toString(standerdChildsAccommodationTypes);
			maximumAdultsAccommodationTypes = (int) AccommodationTypes.getRow(6).getCell(1).getNumericCellValue();
			maximumAdultsAccommodationTypesAsString = Integer.toString(maximumAdultsAccommodationTypes);
			allowedGenderAccommodationTypes =  AccommodationTypes.getRow(7).getCell(1).getStringCellValue();
			descriptionAccommodationTypes = AccommodationTypes.getRow(8).getCell(1).getStringCellValue();
			amenityAccommodationTypes = AccommodationTypes.getRow(9).getCell(1).getStringCellValue();
			
			
			HashMap<String, String> objMap = new HashMap<String, String>();
		    objMap.put("A", "A");
		    objMap.put("klasasB", "B");
		    objMap.put("asasC", "C");
		    objMap.put("bgbghA", "D");
		    System.out.println("Elements of the Map:");
		    System.out.println(objMap);
		    Set<String> asd = objMap.keySet();
		    for(String asd_new:asd){
		    	System.out.println(asd_new);
		    }
		}
	
	
		
			catch (Exception e) {
			System.out.println(e.getMessage());
			}
	}
}

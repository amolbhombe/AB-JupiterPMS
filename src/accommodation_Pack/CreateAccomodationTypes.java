package accommodation_Pack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import genericPack.LibGenericClass;
import genericPack.ReadGenericData;
import jupiter_Pack1.Jupiter;
import jupiter_Pack1.LibClassJupiter;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class CreateAccomodationTypes {
	
	WebDriverWait wait;
	WebDriver driver;
	ReadGenericData getdata;
	String str;
	int sizeOfWebelement;
	
	String menuWhile;
	String menuWeWant;
	List<WebElement> allSettingMenu;
	
	
	By SettingsMenu = By.xpath("//aside[@class='main-sidebar']//ul//li//a//span");
	By accommodationTypesMenu = By.xpath("//span[contains(text(),'Extras')]");
	By accommodationTypesCreateButton = By.xpath("//a[contains(text(),'Create Extra')]");
	By createButton = By.xpath("//button[@class='btn btn-success'][contains(text(),'Create')]");
			
			@Test
			public void test() throws IOException, InterruptedException{
				
				getdata = new ReadGenericData();
				System.setProperty("webdriver.chrome.driver", "/home/amol/Music/AmolRRRR/JavaProjects/AB-JupiterPMS/chromedriver");
				driver = new ChromeDriver();
				wait = new WebDriverWait(driver, 3);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				driver.get("http://localhost/TH-Jupiter-PMS/web/index.php?r=site%2Flogin");
				driver.findElement(By.name("LoginForm[username]")).sendKeys("amolb@techhighway.co.in");
				driver.findElement(By.name("LoginForm[password]")).sendKeys("th@123");
				driver.findElement(By.name("login-button")).click();
				
				Logger logger = Logger.getLogger("LibGenericClass");
				PropertyConfigurator.configure("log4j.properties");
//				logger.info("Coming from logger");
				
				
				Set<String> keysetOfMenu = getdata.menuRead().keySet();//get all the keys from Menu name excel
				Iterator<String> itrMenu = keysetOfMenu.iterator();
				while(itrMenu.hasNext()){
					menuWhile =  itrMenu.next();
			//		System.out.println("Menu is "+menuWhile);
					System.out.println("menu is "+menuWhile);
					logger.info("We are woking on "+menuWhile +" module");
					menuWeWant = getMenuFromExcel(menuWhile);
					if(menuWeWant.equalsIgnoreCase("underSettings")){
						List<WebElement> allSettingMenu = driver.findElements(SettingsMenu);
						Iterator<WebElement> web = allSettingMenu.iterator();
						while(web.hasNext()){
							WebElement ele_Settings = web.next();
								String str_settings = ele_Settings.getText();
								if(str_settings.equalsIgnoreCase("Settings")){
									ele_Settings.click();
									break;
								}
						}
						
					}
							String clickOnMenu = createXpathForMenu(menuWhile);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(clickOnMenu))).click();;//driver.findElement(By.xpath(clickOnMenu));//
																				
							
							String clickOnCreateButton = createXpathForCreateButton(getdata.menuRead().get(menuWhile));
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(clickOnCreateButton))).click();
							
							
							Set<String> keyset = getdata.read().keySet();//This code get all the Keys (i.e. all the name)
							Iterator<String> itrKeySet= keyset.iterator();
								while(itrKeySet.hasNext()){
									String gettingKeys = itrKeySet.next();// Here we get the keys
									System.out.println("Getting key is "+gettingKeys);
									sizeOfWebelement = driver.findElements(By.name(gettingKeys)).size();
									
									//we check the tagname if it is input and the type is text (i.e. Text field)then this code execute
									
									if(sizeOfWebelement !=0){
									//	System.out.println("Name is "+gettingKeys);
			//							logger.info("The name is "+gettingKeys);
										WebElement keyWeBelement =	driver.findElement(By.name(gettingKeys));
										String tagName = keyWeBelement.getTagName(); //from the keys we found the tag name 
										String attribute = keyWeBelement.getAttribute("type");
										String classname = keyWeBelement.getAttribute("class");
										
										switch(tagName){
										case "select": 
											String clickElements = clickConverter(gettingKeys,"click");//here we get the id of the select 2 dropdown from the select drop down
											System.out.println("The drop down ID id "+clickElements);
											driver.findElement(By.id(clickElements)).click(); //we click on drop down
											String gettingElements = clickConverter(gettingKeys,"getting");//here we get id from which we cab read all the elements
											String xpathConvetredString = xpathforDropDown(gettingElements); //we create the xpath to read all the elements
											List<WebElement> xpathConvetredStringWeb = driver.findElements(By.xpath(xpathConvetredString));
											Iterator<WebElement> itr = xpathConvetredStringWeb.iterator();
											while(itr.hasNext()){
													WebElement ele = itr.next();
													String code1 = 	ele.getText();
													if(code1.equalsIgnoreCase(getdata.read().get(gettingKeys))){//here we search a specific text from which we can click on specific menu
													ele.click();
														break;
													}
												}
														break;
										case "input": 
														switch(attribute){
														case "text":
																	switch(classname){
																	case "form-control": wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
																						 break;
																	case "form-control krajee-datepicker": driver.findElement(By.name(gettingKeys)).click();
																											List<WebElement> calender = driver.findElements(By.xpath("//table/tbody//tr//td"));
																											Iterator<WebElement> itrCalender = calender.iterator();
																											while(itrCalender.hasNext()){
																												WebElement WebelementCalnder = itrCalender.next();
																												String calenderStringelement = WebelementCalnder.getText();
																													if(calenderStringelement.equalsIgnoreCase(getdata.read().get(gettingKeys))){
																													String date = WebelementCalnder.getText();
																													wait.until(ExpectedConditions.elementToBeClickable(WebelementCalnder)).click();
																													try {
																														Thread.sleep(2000);
																													} catch (InterruptedException e) {
																														// TODO Auto-generated catch block
																														e.printStackTrace();
																													}
																													break;
																												}
																											}
																													break;
																	}
																	break;
														case "hidden":
																		switch(getdata.read().get(gettingKeys)){
																		case "No":  String xpathActive = xpathforActiveToggle(gettingKeys);
																					WebElement eleforDropDown = driver.findElement(By.xpath(xpathActive));
																					JavascriptExecutor executor = (JavascriptExecutor)driver;
																					executor.executeScript("arguments[0].click();", eleforDropDown);
																					break;
																	}
																	break;
														case "number":  wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
																	break;
																		
														case "password": wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field							
																	break;
														}
																	break;
										default : System.out.println("I am in default");
										}
										/*if(tagName.equalsIgnoreCase("input") && attribute.equalsIgnoreCase("text") && classname.equalsIgnoreCase("form-control")){
											wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
											continue;
										}
											//if tagname is select(i.e. Select 2 drop down) then this code is execute
											else if(tagName.equalsIgnoreCase("select")){
											String clickElements = clickConverter(gettingKeys,"click");//here we get the id of the select 2 dropdown from the select drop down
											driver.findElement(By.id(clickElements)).click(); //we click on drop down
											String gettingElements = clickConverter(gettingKeys,"getting");//here we get id from which we cab read all the elements
											String xpathConvetredString = xpathforDropDown(gettingElements); //we create the xpath to read all the elements
											List<WebElement> xpathConvetredStringWeb = driver.findElements(By.xpath(xpathConvetredString));
											Iterator<WebElement> itr = xpathConvetredStringWeb.iterator();
												while(itr.hasNext()){
														WebElement ele = itr.next();
														String code1 = 	ele.getText();
														if(code1.equalsIgnoreCase(getdata.read().get(gettingKeys))){//here we search a specific text from which we can click on specific menu
														ele.click();
														break;
														}
													}
											
											}	
												//if tagname is input and type is hidden and also status keep should be Inactive(i.e. Active toggle) then this code works
												else if(tagName.equalsIgnoreCase("input") && getdata.read().get(gettingKeys).equalsIgnoreCase("No") && attribute.equalsIgnoreCase("hidden")){
													String xpathActive = xpathforActiveToggle(gettingKeys);
													WebElement eleforDropDown = driver.findElement(By.xpath(xpathActive));
													JavascriptExecutor executor = (JavascriptExecutor)driver;
													executor.executeScript("arguments[0].click();", eleforDropDown);
													continue;
												}
													//we check the tagname if it is input and type is "number"(i.e. number field)then this code execute
													else if(attribute.equalsIgnoreCase("number") && tagName.equalsIgnoreCase("input")){
														wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
														continue;
													}
														//we check the tag name if it is input and type is "password"(i.e. Password field) then this code works
														else if(attribute.equalsIgnoreCase("password") && tagName.equalsIgnoreCase("input")){
																wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
																continue;
															}
															else if(classname.equalsIgnoreCase("form-control krajee-datepicker") && tagName.equalsIgnoreCase("input")){
																driver.findElement(By.name(gettingKeys)).click();
																List<WebElement> calender = driver.findElements(By.xpath("//table/tbody//tr//td"));
																Iterator<WebElement> itrCalender = calender.iterator();
																while(itrCalender.hasNext()){
																	WebElement WebelementCalnder = itrCalender.next();
																	String calenderStringelement = WebelementCalnder.getText();
																		if(calenderStringelement.equalsIgnoreCase(getdata.read	().get(gettingKeys))){
																		String date = WebelementCalnder.getText();
																		wait.until(ExpectedConditions.elementToBeClickable(WebelementCalnder)).click();
																		try {
																			Thread.sleep(2000);
																		} catch (InterruptedException e) {
																			// TODO Auto-generated catch block
																			e.printStackTrace();
																		}
																		break;
																	}
																}
																continue;
															}*/
									
												}else{
													continue;
												}
									}						
									JavascriptExecutor demo = (JavascriptExecutor)driver;
									demo.executeScript("scroll(0, 800)");
									wait.until(ExpectedConditions.elementToBeClickable(createButton)).click();//Here we click on create button
									
									
									
									
									Thread.sleep(2000);
				//			System.out.println("Page title is "+ driver.getTitle());
									logger.info("Page title is "+driver.getTitle());
									if(driver.getTitle().equalsIgnoreCase(getdata.menuRead().get(menuWhile))){
										logger.info("Create functionality of " +menuWhile +" is not working");
									}
									else{
										logger.info("Create functionality of " +menuWhile +" is working fine");
									}
									demo.executeScript("scroll(800,0)");
									if(menuWeWant.equalsIgnoreCase("underSettings")){
										allSettingMenu = driver.findElements(SettingsMenu);
										Iterator<WebElement> web = allSettingMenu.iterator();
										while(web.hasNext()){
											WebElement ele_Settings = web.next();
												String str_settings = ele_Settings.getText();
												if(str_settings.equalsIgnoreCase("Settings")){
													ele_Settings.click();
													break;
												}
										}
										
									}
											Thread.sleep(2000);
				}
			}
		
			
			public String getMenuFromExcel(String accessMenu){
				str = "outSideofSettings";
				ArrayList<String> sortMenu = new ArrayList<String>();
				sortMenu.add("Menus");
				sortMenu.add("Role");
				sortMenu.add("Menus Roles Permission");
				sortMenu.add("Accommodation Types");
				sortMenu.add("Cancellation Policie");
				sortMenu.add("Card Type");
				sortMenu.add("Charge Type");
				sortMenu.add("Item Categories");
				sortMenu.add("Payment Options");
				sortMenu.add("Permissions");
				sortMenu.add("Statuses");
				sortMenu.add("Parameters");
				sortMenu.add("General Settings");
				
				Iterator<String> itrgetMenuFromExcel = sortMenu.iterator();
				while(itrgetMenuFromExcel.hasNext()){
					String getStr = itrgetMenuFromExcel.next();
					if(accessMenu.equalsIgnoreCase(getStr)){
						str = "underSettings";
						break;
					}
				}
				return str;
			}
	
					public String createXpathForMenu(String clickOnMenu){
						////span[contains(text(),'Accommodation Types')]
						// Accommodation Types
						//clickOnMenu = ""//input[@id='"+xpathtoggle+"']/preceding::span[3]"";
						clickOnMenu = "//span[contains(text(),'"+clickOnMenu+"')]";
						return clickOnMenu;
					}
			
			
					public String createXpathForCreateButton(String clickOnCreateButton){
						//By.xpath("//a[contains(text(),'Create Accommodation Type')]")
						clickOnCreateButton = "//a[contains(text(),'"+clickOnCreateButton+"')]";
						return clickOnCreateButton;
					}
					
					//This function found the drop down id, on which we want to click also create and also found  all the elements id from the drop down 
					public String clickConverter(String replaceString,String whichString){
						String str = replaceString;
				        str = str.replaceAll("\\[", "-");
				        str = str.replaceAll("\\]", "-");
				        str = str.toLowerCase();
						        if(whichString.equalsIgnoreCase("click")){
						        str = "select2-"+str+"container";
						        }
						        else{
						        	 str = "select2-"+str+"results";
						        }
						        System.out.println("ClickConverter methd return "+str);
				        return str;
					}
					
					//This function create the xpath to select all the elements from the drop down
					public String xpathforDropDown(String xpathstr){
						xpathstr = "//span//ul[@id='" + xpathstr + "']/li";
						return xpathstr;
					}
					
					//This function create the xpath for Active toggle 
					public String xpathforActiveToggle(String xpathtoggle){
				        xpathtoggle = xpathtoggle.replaceAll("\\[", "-");
				        xpathtoggle = xpathtoggle.replaceAll("\\]", "");
				        xpathtoggle = xpathtoggle.toLowerCase();
				        xpathtoggle = "//input[@id='"+xpathtoggle+"']/preceding::span[3]";
				        return xpathtoggle;
					}
			
	}
			

////a[contains(text(),'Create Accommodation Type')]
////a[contains(text(),'Create Accommodation Type']
/*

//This code click on Settings menu
List<WebElement> allSettingMenu = driver.findElements(SettingsMenu);
Iterator<WebElement> web = allSettingMenu.iterator();
while(web.hasNext()){
	WebElement ele_Settings = web.next();
		String str_settings = ele_Settings.getText();
		if(str_settings.equalsIgnoreCase("Settings")){
			ele_Settings.click();
		}
}

driver.findElement(accommodationTypesMenu).click();
wait = new WebDriverWait(driver, 3);
WebElement create1 = wait.until(ExpectedConditions.elementToBeClickable(accommodationTypesCreateButton));
create1.click();

Set<String> keyset = getdata.read().keySet();//This code get all the Keys (i.e. all the name)
Iterator<String> itrKeySet= keyset.iterator();
	while(itrKeySet.hasNext()){
		String gettingKeys = itrKeySet.next();// Here we get the keys
		WebElement keyWeBelement =	driver.findElement(By.name(gettingKeys));
		String tagName = keyWeBelement.getTagName(); //from the keys we found the tag name 
		String attribute = keyWeBelement.getAttribute("type");
		String classname = keyWeBelement.getAttribute("class");
		//we check the tagname if it is input and the type is text (i.e. Text field)then this code execute
		if(tagName.equalsIgnoreCase("input") && attribute.equalsIgnoreCase("text") && classname.equalsIgnoreCase("form-control")){
			wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
		}
			//if tagname is select(i.e. Select 2 drop down) then this code is execute
			else if(tagName.equalsIgnoreCase("select")){
			String clickElements = clickConverter(gettingKeys,"click");//here we get the id of the select 2 dropdown from the select drop down
			driver.findElement(By.id(clickElements)).click(); //we click on drop down
			String gettingElements = clickConverter(gettingKeys,"getting");//here we get id from which we cab read all the elements
			String xpathConvetredString = xpathforDropDown(gettingElements); //we create the xpath to read all the elements
			List<WebElement> xpathConvetredStringWeb = driver.findElements(By.xpath(xpathConvetredString));
			Iterator<WebElement> itr = xpathConvetredStringWeb.iterator();
				while(itr.hasNext()){
						WebElement ele = itr.next();
						String code1 = 	ele.getText();
						if(code1.equalsIgnoreCase(getdata.read().get(gettingKeys))){//here we search a specific text from which we can click on specific menu
						ele.click();
						break;
						}
					}
			}	
				//if tagname is input and type is hidden and also status keep should be Inactive(i.e. Active toggle) then this code works
				else if(tagName.equalsIgnoreCase("input") && attribute.equalsIgnoreCase("hidden") && getdata.read().get(gettingKeys).equalsIgnoreCase("No")){
					String xpathActive = xpathforActiveToggle(gettingKeys);
					WebElement eleforDropDown = driver.findElement(By.xpath(xpathActive));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", eleforDropDown);
				}
					//we check the tagname if it is input and type is "number"(i.e. number field)then this code execute
					else if(tagName.equalsIgnoreCase("input") && attribute.equalsIgnoreCase("number")){
						wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
					}
						//we check the tag name if it is input and type is "password"(i.e. Password field) then this code works
						else if(tagName.equalsIgnoreCase("input") && attribute.equalsIgnoreCase("password")){
								wait.until(ExpectedConditions.elementToBeClickable(keyWeBelement)).sendKeys(getdata.read().get(gettingKeys));//Here we enter the text in input field
							}
							else if(tagName.equalsIgnoreCase("input") && classname.equalsIgnoreCase("form-control krajee-datepicker")){
								driver.findElement(By.name(gettingKeys)).click();
								List<WebElement> calender = driver.findElements(By.xpath("//table/tbody//tr//td"));
								Iterator<WebElement> itrCalender = calender.iterator();
								while(itrCalender.hasNext()){
									WebElement WebelementCalnder = itrCalender.next();
									String calenderStringelement = WebelementCalnder.getText();
										if(calenderStringelement.equalsIgnoreCase(getdata.read().get(gettingKeys))){
										String date = WebelementCalnder.getText();
										wait.until(ExpectedConditions.elementToBeClickable(WebelementCalnder)).click();
										Thread.sleep(2000);
										break;
									}
								}
							}
			
		}						
		JavascriptExecutor demo = (JavascriptExecutor)driver;
		demo.executeScript("scroll(0, 800)");
		wait.until(ExpectedConditions.elementToBeClickable(createButton)).click();//Here we click on create button
*/